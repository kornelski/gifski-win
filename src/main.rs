#![allow(unused)]

#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]
use gifski::Settings;
use tauri::api::dialog::FileDialogBuilder;
use once_cell::sync::OnceCell;
use tauri::api::dialog::{MessageDialogBuilder, confirm};
use tauri::async_runtime::spawn;
use tauri::updater::{EVENT_UPDATE_AVAILABLE, EVENT_STATUS_UPDATE, EVENT_STATUS_ERROR, EVENT_INSTALL_UPDATE, EVENT_CHECK_UPDATE, EVENT_STATUS_UPTODATE};
use tauri::{Manager, AppHandle, Menu, Submenu, MenuItem, CustomMenuItem, window, AboutMetadata};
use std::fs::File;
use std::path::{Path, PathBuf};
use std::io::BufWriter;
use std::sync::{Arc, Mutex};
use std::sync::atomic::{AtomicBool, AtomicU8};
use std::sync::atomic::Ordering::SeqCst;

struct Session {
    aborted: AtomicBool,
}

impl Session {
    fn new() -> Self {
        Self { aborted: AtomicBool::new(false) }
    }

    fn is_aborted(&self) -> bool {
        self.aborted.load(SeqCst)
    }

    fn abort(&self) {
        self.aborted.store(true, SeqCst)
    }

    fn fail(&self, why: &str) {
        self.abort();
        if let Some(app) = APP.get() {
            app.emit_all("fail", why);
        }
        MessageDialogBuilder::new("Failed", why)
            .kind(tauri::api::dialog::MessageDialogKind::Error)
            .show(|_| {});
    }

    fn emit_progress(&self, percent: u32) {
        if let Some(app) = APP.get() {
            app.emit_all("progress", percent);
        }
    }
}

struct Progress {
    done: u32,
    num_frames: u32,
    session: Arc<Session>
}

impl gifski::progress::ProgressReporter for Progress {
    fn increase(&mut self) -> bool {
        let last_done = self.done * 100 / self.num_frames;
        self.done += 1;
        let this_done = self.done * 100 / self.num_frames;
        if this_done != last_done {
            self.session.emit_progress(this_done);
        }
        !self.session.is_aborted()
    }

    fn done(&mut self, _: &str) {} // nope
}

#[tauri::command]
fn file_drop(paths: Vec<String>) {
    let sess = Arc::new(Session::new());
    if let Err(e) = process(paths, sess.clone()) {
        sess.fail(&e);
    }
}

fn process(mut paths: Vec<String>, sess: Arc<Session>) -> Result<(), String> {
    if paths.is_empty() {
        return Ok(());
    }

    if let [only] = &paths[..] {
        if Path::new(only).is_dir() {
            paths = std::fs::read_dir(only)
                .map_err(|e| e.to_string())?
                .filter_map(|e| {
                    let e = e.ok()?;
                    let path = e.path();
                    if "png".eq_ignore_ascii_case(path.extension()?.to_str()?) {
                        Some(path.to_str()?.to_string())
                    } else {
                        None
                    }
                })
                .collect();
        }
    }

    if paths.len() < 2 {
        return Err("You have to drop ALL frames at once, and they must be in PNG format".into());
    }

    paths.sort_by(|a, b| natord::compare(a, b));

    let file_stem = Path::new(&paths[0]).file_stem().and_then(|f| f.to_str())
        .unwrap_or("anim").trim_end_matches(|c: char| c.is_ascii_digit());
    let dest_file_name = format!("{file_stem}.gif");
    let dest_dir = Path::new(&paths[0]).parent().map(|p| p.to_path_buf());

    let mut progress = Progress {
        num_frames: paths.len() as _,
        done: 0,
        session: sess.clone(),
    };

    let (mut c, w) = gifski::new(Settings {
        quality: QUALITY.load(SeqCst),
        ..Settings::default()
    }).map_err(|e| e.to_string())?;

    let a = sess.clone();
    std::thread::spawn(move || {
        let fps = FPS.load(SeqCst) as f64;
        for (i, path) in paths.into_iter().enumerate() {
            if a.is_aborted() {
                break;
            }

            if let Err(e) = c.add_frame_png_file(i, PathBuf::from(path), i as f64 / fps) {
                a.fail(&e.to_string());
                return;
            }
        }
    });

    let mut b = FileDialogBuilder::new()
        .set_file_name(&dest_file_name);

    if let Some(dir) = dest_dir {
        b = b.set_directory(dir);
    }

    b.set_title("Where to save the GIF")
    .save_file(move |path| {
        if sess.is_aborted() {
            return;
        }
        let path = match path {
            Some(path) => path,
            None => {
                sess.fail("Aborted");
                return;
            }
        };
        sess.emit_progress(0);
        std::thread::spawn(move || {
            let res = File::create(&path).map(BufWriter::new)
            .map_err(gifski::Error::from)
            .and_then(move |f| {
                w.write(f, &mut progress)
            });
            match res {
                Ok(()) => {
                    let file_name = path.file_name().and_then(|f| f.to_str()).unwrap_or("gif");
                    if let Some(app) = APP.get() {
                        app.emit_all("done", file_name);
                    }
                },
                Err(e) => {
                    sess.fail(&e.to_string());
                },
            }
        });
    });
    Ok(())
}

static FPS: AtomicU8 = AtomicU8::new(20);
static QUALITY: AtomicU8 = AtomicU8::new(100);
static APP: OnceCell<AppHandle> = OnceCell::new();

fn menu_notify(msg: &str) {
    if let Some(app) = APP.get() {
        app.emit_all("menu", msg);
    }
}

fn main() {
    tauri::Builder::default()
        .setup(|app| {
            let handle = app.handle();
            if let Some(window) = handle.get_window("gifski") {
                window.listen(EVENT_STATUS_UPDATE, move |msg| {
                    eprintln!("Update status: {:?}", msg);
                });
                let handle = handle.clone();
                window.clone().listen(EVENT_UPDATE_AVAILABLE, move |msg| {
                    eprintln!("New version available: {:?}", msg);
                    let window = window.clone();
                    confirm(Some(&window.clone()), "gif.ski update available", "A new version of gif.ski will be installed now", move |okay| {
                        if okay {
                            window.emit(EVENT_INSTALL_UPDATE, ());
                        }
                    });
                });
            }
            APP.set(handle).unwrap();
            Ok(())
        })
        .menu(Menu::with_items([
            Submenu::new("App", Menu::with_items([
                #[cfg(target_os = "linux")]
                MenuItem::About(
                    format!("gif.ski"),
                    AboutMetadata::new()
                        .authors(vec!["Kornel Lesiński".into()])
                        .version(env!("CARGO_PKG_VERSION"))
                        .license("AGPL")
                        .website("https://gif.ski")
                        .website_label("gif.ski"),
                ).into(),
                #[cfg(not(target_os = "linux"))]
                CustomMenuItem::new("about", "About").into(),
                CustomMenuItem::new("upd", "Check for updates").into(),
                CustomMenuItem::new("lic", "License notice").into(),

                #[cfg(target_os = "macos")]
                MenuItem::Separator.into(),
                #[cfg(target_os = "macos")]
                MenuItem::Hide.into(),
                #[cfg(target_os = "macos")]
                MenuItem::HideOthers.into(),

                MenuItem::Separator.into(),
                MenuItem::Quit.into(),
            ])).into(),
            Submenu::new("File", Menu::with_items([
                CustomMenuItem::new("open", "Open…").into(),
            ])).into(),
            Submenu::new("FPS", Menu::with_items([
                CustomMenuItem::new("f50", "50 fps").into(), // menu selection is wonky ;(
                CustomMenuItem::new("f20", "20 fps").into(),
                CustomMenuItem::new("f10", "10 fps").into(),
                CustomMenuItem::new("f5", "5 fps").into(),
                CustomMenuItem::new("f1", "1 fps").into(),
            ])).into(),
            Submenu::new("Quality", Menu::with_items([
                CustomMenuItem::new("q100", "Best").into(),
                CustomMenuItem::new("q90", "Great").into(),
                CustomMenuItem::new("q80", "Good").into(),
                CustomMenuItem::new("q70", "OK").into(),
                CustomMenuItem::new("q50", "Meh").into(),
                CustomMenuItem::new("q30", "Potato").into(),
            ])).into(),
        ]))
        .on_menu_event(|event| {
            match event.menu_item_id() {
                "upd" => {
                    if let Some(window) = APP.get().unwrap().get_window("gifski") {
                        let h = APP.get().unwrap().clone();
                        tauri::async_runtime::spawn(async move {
                            match tauri::updater::builder(h).check().await {
                                Ok(update) => {
                                    eprintln!("Updater found latest is {} {}", update.latest_version(), update.body().map(|s| s.as_str()).unwrap_or_default());
                                    if update.is_update_available() {
                                        let (reply, confirmation) = tokio::sync::oneshot::channel();
                                        confirm(Some(&window.clone()), "gif.ski update available", "A new version of gif.ski will be installed now", move |okay| {
                                            reply.send(okay);
                                        });
                                        if confirmation.await.unwrap_or(false) {
                                            update.download_and_install().await.unwrap();
                                        } else {
                                            open_url("https://gif.ski/#update");
                                        }
                                    } else {
                                        tauri::api::dialog::message(Some(&window), "Up to date!", "You have the latest version of gif.ski for Windows");
                                    }
                                },
                                Err(e) => {
                                    tauri::api::dialog::message(Some(&window), "Update check failed", format!("{e}\nYou can download the update from https://gif.ski"));
                                },
                            }
                        });
                    }
                },
                "lic" => {
                    open_url("https://gif.ski/license.html");
                },
                "open" => {
                    FileDialogBuilder::new()
                        .set_title("Select all PNG frames")
                        .add_filter("PNG images", &["png"])
                        .pick_files(move |files| {
                            let app = &APP.get().unwrap();
                            if let Some(files) = files {
                                file_drop(files.into_iter().filter_map(|f| f.into_os_string().into_string().ok()).collect());
                            }
                        });
                },
                "about" => {
                    tauri::api::dialog::message(Some(event.window()), format!("gif.ski {}", env!("CARGO_PKG_VERSION")), "Created by Kornel Lesiński\nFree software under AGPL");
                },
                id => {
                    if let Some(fps) = id.strip_prefix('f') {
                        FPS.store(fps.parse().unwrap(), SeqCst);
                        menu_notify(&format!("{fps} fps"));
                    }
                    else if let Some(quality) = id.strip_prefix('q') {
                        QUALITY.store(quality.parse().unwrap(), SeqCst);
                        menu_notify(&format!("Quality {quality}%"));
                    }
                }
            };
        })
        .invoke_handler(tauri::generate_handler![file_drop])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}

fn open_url(url: &str) {
    tauri::api::shell::open(&APP.get().unwrap().shell_scope(), url, None);
}
